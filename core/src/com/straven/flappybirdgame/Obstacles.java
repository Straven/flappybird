package com.straven.flappybirdgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.security.SecureRandom;

public class Obstacles {
    static class WallPair {
        Vector2 position;
        float speed;
        int offset;
        Rectangle emptySpace;

        public WallPair(Vector2 pos) {
            position = pos;
            speed = 2;
            offset = randomOffset(250);
            emptySpace = new Rectangle(position.x, position.y - offset + 300, 50, betweenDistance);
        }

        public void update() {
            position.x -= speed;
            if (position.x < -50) {
                position.x = 800;
                offset = randomOffset(250);
            }
            emptySpace.x = position.x;
        }

        public int randomOffset(int nextInt) {
            offset = new SecureRandom().nextInt(nextInt);
            return offset;
        }
    }

    static WallPair[] obs;
    Texture txt;
    static int betweenDistance = 250;

    public Obstacles() {
        txt = new Texture("wall.png");
        obs = new WallPair[4];
        int startPositionX = 400;
        for (int i = 0; i < obs.length; i++) {
            obs[i] = new WallPair(new Vector2(startPositionX, 0));
            startPositionX += 220;
        }
    }

    public void render(SpriteBatch batch) {
        for (WallPair ob : obs) {
            batch.draw(txt, ob.position.x, ob.position.y - ob.offset);
            batch.draw(txt, ob.position.x, ob.position.y + betweenDistance + txt.getHeight() - ob.offset);
        }
    }

    public void update() {
        for (WallPair ob : obs) {
            ob.update();
        }
    }

    public static void recreate() {
        int startPosX = 400;
        for (int i = 0; i < obs.length; i++) {
            obs[i] = new WallPair(new Vector2(startPosX, 0));
            startPosX += 220;
        }
    }
}
