package com.straven.flappybirdgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Background {

    static class BGPicture {
        private final Texture tx;
        private final Vector2 pos;

        public BGPicture(Vector2 pos) {
            tx = new Texture("back.png");
            this.pos = pos;
        }
    }

    private final int speed;
    private final BGPicture[] back;

    public Background() {
        speed = 4;
        back = new BGPicture[2];
        back[0] = new BGPicture(new Vector2(0,0));
        back[1] = new BGPicture(new Vector2(800,0));
    }

    public void render(SpriteBatch batch) {
        for (BGPicture bgPicture : back) {
            batch.draw(bgPicture.tx, bgPicture.pos.x, bgPicture.pos.y);
            batch.draw(bgPicture.tx, bgPicture.pos.x + bgPicture.tx.getWidth(), bgPicture.pos.y);
        }
    }

    public void update() {
        for (BGPicture bgPicture : back) {
            bgPicture.pos.x -= speed;
        }

        if (back[0].pos.x < -800) {
            back[0].pos.x = 0;
            back[1].pos.y = 800;
        }
    }
}
